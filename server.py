#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import os


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Clase Handle."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        # self.wfile.write(b"Hemos recibido tu peticion\r\n")
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("Peticion " + line.decode('utf-8'))
            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break

            var = line.decode('utf-8')
            vari = var.split(" ")
            if vari[0] == 'INVITE':
                mandar = b"SIP/2.0 100 TRYING\r\n\r\n"
                mandar1 = mandar + b"SIP/2.0 180 RING\r\n\r\n"
                mandar2 = mandar1 + b"SIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(mandar2)
            elif vari[0] == 'ACK':
                aEjecutar = 'mp32rtp -i 127.0.0.1 -p 23032 <' + ARCHIVO
                print("Vamos a ejecutar", aEjecutar)
                os.system(aEjecutar)
            elif vari[0] == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        IP = (sys.argv[1])
        PORT = int(sys.argv[2])
        ARCHIVO = (sys.argv[3])

    except IndexError:
        sys.exit("Usage: python3 server.py IP port audio_file")

    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Listening...")
    serv.serve_forever()
