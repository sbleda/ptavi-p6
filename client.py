#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

if __name__ == "__main__":
    try:
        RECEPTOR = str(sys.argv[2])
        METODO = str(sys.argv[1])
    except IndexError:
        sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
    except ValueError:
        sys.exit("SIP/2.0 400 Bad Request")
    SERVER = 'localhost'
    receptor1 = RECEPTOR.split(":")
    receptor2 = receptor1[0].split("@")
    login = receptor2[0]
    IP = (receptor2[1])
    PORT = int(receptor1[1])

    if METODO == 'INVITE' or 'BYE':
        linea = (METODO + " sip:" + receptor1[0] + " SIP/2.0\r\n")
    else:
        linea = ' '

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))

    print("Enviando: " + linea)
    my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido:' + data.decode('utf-8'))
    if METODO == 'INVITE':
        a = "SIP/2.0 100 TRYING"
        b = "SIP/2.0 180 RING"
        c = "SIP/2.0 200 OK"
        x = data.decode('utf-8')
        y = x.split('\r\n\r\n')
        if y[0] == a:
            if y[1] == b:
                if y[2] == c:
                    METODO = 'ACK'
                    ack = (METODO + " sip:" + receptor1[0] + " SIP/2.0\r\n")
                    my_socket.send(bytes(ack, 'utf-8') + b'\r\n')
    print("Terminando socket...")
print("Fin.")
